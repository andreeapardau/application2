package com.example.application2;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;


import java.util.List;


public class TaskList extends ArrayAdapter<Task> {
    private Activity context;
    List<Task> tasks;


    public TaskList(Activity context, List<Task> tasks){

        super(context, R.layout.list_tasks_layout, tasks);
        this.context = context;
        this.tasks = tasks;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.list_tasks_layout,null,true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);

        Task task = tasks.get(position);
        textViewName.setText(task.getTaskName());

        return listViewItem;



    }

}

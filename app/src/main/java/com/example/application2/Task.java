package com.example.application2;

public class Task {

    private String id;
    private String taskName;

    public Task(){


    }

    public Task (String id, String taskName){
        this.taskName = taskName;
        this.id = id;

    }

    public String getId() {
        return id;
    }

    public String getTaskName() {
        return taskName;
    }
}

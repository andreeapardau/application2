package com.example.application2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import io.grpc.okhttp.internal.Util;

public class LoginActivity extends AppCompatActivity {


    private TextInputLayout editTextEmail, editTextPassword;

    private FirebaseAuth mAuth;
    private View btnLogin, btnRegister;
    private ProgressDialog progressDialog;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextEmail = findViewById(R.id.edit_text_email);
        editTextPassword = findViewById(R.id.edit_text_password);
        progressBar = findViewById(R.id.progressbar);
        //progressBar.setVisibility(View.GONE);

        btnLogin = findViewById(R.id.login);
        btnRegister = findViewById(R.id.register);

        mAuth = FirebaseAuth.getInstance();

        //go to Register Activity
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.hasText(editTextEmail)) {
                    Utils.showToast(LoginActivity.this, "Please input your email");
                } else if (!Utils.hasText(editTextPassword)) {
                    Utils.showToast(LoginActivity.this, "Please input your password");
                } else {

                    showProcessDialog();

                    authenticateUser(Utils.getText(editTextEmail), Utils.getText(editTextPassword));
                }
            }
        });
    }

    private void authenticateUser(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Utils.showToast(LoginActivity.this, "Login error");
                        } else {
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            progressDialog.dismiss();
                            //progressBar.setVisibility(View.GONE);
                            finish();
                        }
                    }
                });

    }

    private void showProcessDialog(){

        progressDialog=new ProgressDialog(this);
        progressDialog.setTitle("Login");
        progressDialog.setMessage("Logging in Firebase server...");
        progressDialog.show();

    }
}



package com.example.application2;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.ArrayList;
import java.util.List;



public class Projects extends AppCompatActivity {


    public static final String PROJECT_ID="projectid";
    public static final String PROJECT_NAME="projectname";



    private EditText mValueField;
    private Button mAddBtn;
    private View btnInitialPage;

    //the database reference object
    DatabaseReference databaseProjects;



    ListView listViewProjects;

    //list to store all the projects from firebase database
    List<appProjects>projectsList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);

        btnInitialPage = findViewById(R.id.btnInitialPage);

        btnInitialPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Projects.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });



        //getting the reference of projects node
        databaseProjects = FirebaseDatabase.getInstance().getReference("Projects");

        //getting views

        mValueField = (EditText) findViewById(R.id.valueField);
        mAddBtn = (Button) findViewById(R.id.addBtn);
        listViewProjects = (ListView)  findViewById(R.id.listViewProjects);

        //list to store projects
        projectsList = new ArrayList<>();

        //adding an onclicklistener to button
            mAddBtn.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){

                    addProject();

                }


            });

        listViewProjects.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l){

                //getting the selected project
                appProjects appProjects = projectsList.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), TaskActivity.class);

                //putting project name and id to intent

                intent.putExtra(PROJECT_ID, appProjects.getProjectID());
                intent.putExtra(PROJECT_NAME, appProjects.getProjectName());

                //starting the activity with intent
                startActivity(intent);
            }

        });


        listViewProjects.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                appProjects appProjects = projectsList.get(i);
                showUpdateDeletePopUp(appProjects.getProjectID(), appProjects.getProjectName());
                return true;
            }
        });




    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseProjects.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                projectsList.clear();

                for (DataSnapshot projectSnapshot : dataSnapshot.getChildren()){
                        appProjects appProjects = projectSnapshot.getValue(appProjects.class);

                        projectsList.add(appProjects);
                }

                ProjectsList adapter = new ProjectsList(Projects.this, projectsList);

                listViewProjects.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void addProject(){
        String name = mValueField.getText().toString().trim();

        //checking if the value is provided
        if(!TextUtils.isEmpty(name)){

            //getting a unique id using push().getkey() method;it will create a unique id and we will use it as the primary key for our Project
                String id = databaseProjects.push().getKey();

                //creating a Project object

                appProjects appProjects = new appProjects(id,name);

                //saving the project
                
                databaseProjects.child(id).setValue(appProjects);

                //setting edittext to blank again
                mValueField.setText("");

                //display a succes toast

                Toast.makeText(this, "Project Added", Toast.LENGTH_LONG).show();


                
        }else {
            Toast.makeText(this, "You should add a project name", Toast.LENGTH_LONG).show();

        }



    }

    private void updateProject(String id, String name){
        //get the specified project reference
        DatabaseReference databaseProjects = FirebaseDatabase.getInstance().getReference("Projects").child(id);

        //updating project
        appProjects appProjects = new appProjects(id, name);
        databaseProjects.setValue(appProjects);
        Toast.makeText(this,"Project Updated", Toast.LENGTH_LONG).show();



    }

    private void deleteProject(String id){
        //get the specified project reference
        DatabaseReference databaseProjects = FirebaseDatabase.getInstance().getReference("Projects").child(id);
        //getting all the tasks for the specified project
        DatabaseReference databaseTasks = FirebaseDatabase.getInstance().getReference("tasks").child(id);
        //removing the project
        databaseProjects.removeValue();

        databaseTasks.removeValue();

        Toast.makeText(getApplicationContext(),"Project Deleted", Toast.LENGTH_LONG).show();




    }

    private void showUpdateDeletePopUp(final String projectId, String projectName){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater =  getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_project, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextName = (EditText) dialogView.findViewById(R.id.editTextName);


        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.buttonUpdateProject);
        final Button buttonDelete = (Button) dialogView.findViewById(R.id.buttonDeleteProject);


        dialogBuilder.setTitle(projectName);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        buttonUpdate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String name = editTextName.getText().toString().trim();
                if(!TextUtils.isEmpty(name)){
                    updateProject(projectId,name);
                    b.dismiss();

                }

            }

        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deleteProject(projectId);
                b.dismiss();

            }
        });


    }





}

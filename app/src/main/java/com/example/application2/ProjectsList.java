package com.example.application2;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ProjectsList extends ArrayAdapter<appProjects> {

    private Activity context;
    private List<appProjects>projectsList;

    public ProjectsList(Activity context, List<appProjects>projectsList){
        super(context,R.layout.list_project_layout,projectsList);

        this.context = context;
        this.projectsList = projectsList;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.list_project_layout,null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);

        appProjects appProjects = projectsList.get(position);

        textViewName.setText(appProjects.getProjectName());

        return listViewItem;

    }
}

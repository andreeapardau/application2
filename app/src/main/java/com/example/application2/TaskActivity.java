package com.example.application2;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;



public class TaskActivity extends AppCompatActivity {



    View btnProjects;
    Button buttonAddTask;
    EditText editTextTask;
    TextView textViewProject;
    ListView listViewTasks;

    DatabaseReference databaseTasks;

    List<Task> tasks;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        tasks = new ArrayList<>();

        btnProjects = findViewById(R.id.buttonProjects);

        buttonAddTask = (Button) findViewById(R.id.buttonAddTask);
        editTextTask = (EditText) findViewById(R.id.editTextName);
        textViewProject = (TextView) findViewById(R.id.textViewProject);
        listViewTasks = (ListView) findViewById(R.id.listViewTasks);



        Intent intent = getIntent();

        String id = intent.getStringExtra(Projects.PROJECT_ID);

        String name = intent.getStringExtra(Projects.PROJECT_NAME);

        textViewProject.setText(name);

        databaseTasks = FirebaseDatabase.getInstance().getReference("tasks").child(id);




        buttonAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTask();
            }
        });

        //go to Projects Page
        btnProjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TaskActivity.this, Projects.class);
                startActivity(intent);
                finish();
            }
        });


    }

    @Override
    protected void onStart(){
        super.onStart();

        databaseTasks.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                tasks.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    Task task = postSnapshot.getValue(Task.class);
                    tasks.add(task);


                }

                TaskList taskListAdapter = new TaskList(TaskActivity.this,tasks);
                listViewTasks.setAdapter(taskListAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void saveTask(){
        String taskName = editTextTask.getText().toString().trim();
        if(!TextUtils.isEmpty(taskName)){
            String id = databaseTasks.push().getKey();
            Task task = new Task(id, taskName);
            databaseTasks.child(id).setValue(task);
            Toast.makeText(this,"Task saved", Toast.LENGTH_LONG).show();
            editTextTask.setText("");

        }else {
            Toast.makeText(this,"Please enter task name", Toast.LENGTH_LONG).show();

        }


    }



}
